################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/protobuf/nachrichten.pb.c \
../Drivers/protobuf/pb_common.c \
../Drivers/protobuf/pb_decode.c \
../Drivers/protobuf/pb_encode.c 

OBJS += \
./Drivers/protobuf/nachrichten.pb.o \
./Drivers/protobuf/pb_common.o \
./Drivers/protobuf/pb_decode.o \
./Drivers/protobuf/pb_encode.o 

C_DEPS += \
./Drivers/protobuf/nachrichten.pb.d \
./Drivers/protobuf/pb_common.d \
./Drivers/protobuf/pb_decode.d \
./Drivers/protobuf/pb_encode.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/protobuf/%.o Drivers/protobuf/%.su: ../Drivers/protobuf/%.c Drivers/protobuf/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32G484xx -c -I../Core/Inc -I../Drivers/STM32G4xx_HAL_Driver/Inc -I../Drivers/STM32G4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G4xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Drivers-2f-protobuf

clean-Drivers-2f-protobuf:
	-$(RM) ./Drivers/protobuf/nachrichten.pb.d ./Drivers/protobuf/nachrichten.pb.o ./Drivers/protobuf/nachrichten.pb.su ./Drivers/protobuf/pb_common.d ./Drivers/protobuf/pb_common.o ./Drivers/protobuf/pb_common.su ./Drivers/protobuf/pb_decode.d ./Drivers/protobuf/pb_decode.o ./Drivers/protobuf/pb_decode.su ./Drivers/protobuf/pb_encode.d ./Drivers/protobuf/pb_encode.o ./Drivers/protobuf/pb_encode.su

.PHONY: clean-Drivers-2f-protobuf


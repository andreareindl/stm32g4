/**
  ******************************************************************************
  * File Name          : List.h
  * Description        : This file provides code for the configuration
  *                      of the different Lists.
  * Author             : Daniel Wetzel
  ******************************************************************************

  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __List_H
#define __List_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Structs -------------------------------------------------------------------*/
 extern struct participant_node{
	 uint8_t devID;
	 uint16_t V_IN;
	 uint16_t I_IN;
	 uint16_t V_OUT;
	 uint16_t I_OUT;
	 uint8_t Temp;
	 uint8_t SOC;
	 uint8_t SOH;
	 uint8_t BF;
	 uint8_t NW;
	 struct participant_node *next;
	 struct participant_node *previous;
 };

 extern struct election_log{
	 uint8_t prioID;
	 uint8_t devID;
	 uint8_t data;
	 struct election_log *next;
	 struct election_log *previous;
 };
/* Functions Participants-----------------------------------------------------------------*/
 void pl_start(void);
 void pl_insert(uint8_t devID, uint16_t V_IN, uint16_t I_IN, uint16_t V_OUT, uint16_t I_OUT, uint8_t Temp, uint8_t SOC, uint8_t SOH, uint8_t BF, uint8_t NW);
 void pl_sort_insert(uint8_t devID, uint16_t V_IN, uint16_t I_IN, uint16_t V_OUT, uint16_t I_OUT, uint8_t Temp, uint8_t SOC, uint8_t SOH, uint8_t BF, uint8_t NW);
 struct participant_node* pl_getInfo(uint8_t devID);
 struct participant_node* pl_getBest(void);
 struct participant_node* pl_getCoAssCan(void);
 void pl_update(uint8_t devID, uint16_t V_IN, uint16_t I_IN, uint16_t V_OUT, uint16_t I_OUT, uint8_t Temp, uint8_t SOC, uint8_t SOH, uint8_t BF, uint8_t NW);
 void pl_delete(uint8_t devID);
 void pl_delete_all(void);
 void pl_List_ErrorHandler(int error_ID);

 /* Functions Election Log-----------------------------------------------------------------*/
  void el_start(void);
  void el_insert(uint8_t prioID, uint8_t devID, uint8_t data);
  struct election_log* el_getInfo(uint8_t devID);
  void el_delete(uint8_t devID);
  void el_delete_all(void);
  void el_List_ErrorHandler(int error_ID);


#ifdef __cplusplus
}
#endif
#endif /*__ List_H */

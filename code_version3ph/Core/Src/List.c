/*
  ******************************************************************************
  * File Name          : List.c
  * Description        : This file provides code for the configuration
  *                      of the different Lists.
  * Author			   : Daniel Wetzel
  ******************************************************************************
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "List.h"
#include "stdlib.h"
/* Participant List-----------------------------------------------------------*/
struct participant_node* anfang;
struct participant_node* ende;

/* Election Log---------------------------------------------------------------*/
struct election_log* el_anfang;
struct election_log* el_ende;
/*Function Declaration*/


/*Functions Participant List*/
void pl_start(void)
{
	anfang = ende = NULL;
}


void pl_insert(uint8_t devID, uint16_t V_IN, uint16_t I_IN, uint16_t V_OUT, uint16_t I_OUT, uint8_t Temp, uint8_t SOC, uint8_t SOH, uint8_t BF, uint8_t NW)
{
	struct participant_node *zeiger, *zeiger1;
	/* Wurde schon ein Speicher f� den ende-Zeiger bereitgestellt?*/
	if(ende == NULL)
	{
		if((ende = malloc(sizeof(struct participant_node))) == NULL)
		{
			pl_List_ErrorHandler(1);
			return;
		}
	}
	/*Gibt es schon ein Element in der Liste?*/
	if(anfang == NULL)
	{
		/*Erstelle erstes Listenelement*/
		if((anfang = malloc(sizeof(struct participant_node))) == NULL)
		{
			pl_List_ErrorHandler(1);
			return;
		}
		anfang->devID = devID;
		anfang->V_IN = V_IN;
		anfang->I_IN = I_IN;
		anfang->V_OUT = V_OUT;
		anfang->I_OUT = I_OUT;
		anfang->Temp = Temp;
		anfang->SOC = SOC;
		anfang->SOH = SOH;
		anfang->BF = BF;
		anfang->NW = NW;
		anfang->next = NULL;
		ende = anfang;
		ende->previous = NULL;
	}
	/*Neues Element wird hinten angeh�ngt*/
	else
	{
		zeiger=anfang;
		while(zeiger->next !=NULL) zeiger=zeiger->next;
		/*Der neue Wert wird hinten an der Liste angeh�ngt*/
		if((zeiger->next = malloc(sizeof(struct participant_node))) == NULL)
		{
			pl_List_ErrorHandler(1);
			return;
		}
		zeiger1=zeiger;
		zeiger=zeiger->next;
		zeiger->devID = devID;
		zeiger->V_IN = V_IN;
		zeiger->I_IN = I_IN;
		zeiger->V_OUT = V_OUT;
		zeiger->I_OUT = I_OUT;
		zeiger->Temp = Temp;
		zeiger->SOC = SOC;
		zeiger->SOH = SOH;
		zeiger->BF = BF;
		zeiger->NW = NW;
		zeiger->next = NULL;
		ende = zeiger;
		zeiger->previous = zeiger1;
		zeiger1->next = zeiger;
	}
}

void pl_sort_insert(uint8_t devID, uint16_t V_IN, uint16_t I_IN, uint16_t V_OUT, uint16_t I_OUT, uint8_t Temp, uint8_t SOC, uint8_t SOH, uint8_t BF, uint8_t NW)
{
	struct participant_node *zeiger, *zeiger1;

	/*Gibt es schon ein Element in der Liste?*/
	if(anfang==NULL) pl_insert(devID, V_IN, I_IN, V_OUT, I_OUT, Temp, SOC, SOH, BF, NW);
	else
	{
		/* Lauf durch die Liste, bis Platz gefunden ist (devID >) */
		zeiger=anfang;
		while(zeiger != NULL && (zeiger->NW < NW)) zeiger=zeiger->next;
		/*Ist es das letzte Element?*/
		if(zeiger==NULL) pl_insert(devID, V_IN, I_IN, V_OUT, I_OUT, Temp, SOC, SOH, BF, NW);
		else
		{	/*Ist es das erste Element?*/
			if(zeiger == anfang)
			{
				anfang = malloc(sizeof(struct participant_node));
				if(NULL == anfang)
				{
					pl_List_ErrorHandler(1);
					return;
				}
				anfang->devID = devID;
				anfang->V_IN = V_IN;
				anfang->I_IN = I_IN;
				anfang->V_OUT = V_OUT;
				anfang->I_OUT = I_OUT;
				anfang->Temp = Temp;
				anfang->SOC = SOC;
				anfang->SOH = SOH;
				anfang->BF = BF;
				anfang->NW = NW;
				anfang->next = zeiger;
				anfang->previous = NULL;
			}
			else
			{
				zeiger1=anfang;
				while(zeiger1->next != zeiger) zeiger1=zeiger1->next;
				zeiger=malloc(sizeof(struct participant_node));
				if(NULL == zeiger)
				{
					pl_List_ErrorHandler(1);
					return;
				}
				zeiger->devID = devID;
				zeiger->V_IN = V_IN;
				zeiger->I_IN = I_IN;
				zeiger->V_OUT = V_OUT;
				zeiger->I_OUT = I_OUT;
				zeiger->Temp = Temp;
				zeiger->SOC = SOC;
				zeiger->SOH = SOH;
				zeiger->BF = BF;
				zeiger->NW = NW;
				zeiger->next = zeiger1->next;
				zeiger->previous = zeiger1;
				zeiger1->next->previous=zeiger;
				zeiger1->next=zeiger;
			}
		}
	}
}

struct participant_node* pl_getInfo(uint8_t devID)
{
	struct participant_node *zeiger, *zeiger1;
	if(anfang != NULL)
	{
		/*Ist es das erste Element?*/
		if(anfang->devID == devID)
		{
			zeiger = anfang;
			return zeiger;
		}
		/*Ist es das letzte Element?*/
		else if(ende->devID == devID)
			{
				zeiger = ende;
				return ende;
			}
			/*Suche Element in Liste*/
			else
			{
				zeiger = anfang;
				while(zeiger->next != NULL)
				{
					zeiger1 = zeiger->next;
					if(zeiger1->devID == devID)
					{
						return zeiger1;
					}
					zeiger = zeiger1;
				}
			}
	}
	else pl_List_ErrorHandler(3);
	return NULL;
}

struct participant_node* pl_getBest()
{
	return anfang;
}

struct participant_node* pl_getCoAssCan()
{
	struct participant_node* buffer;
	buffer = anfang->next->next;
	return buffer;
}

void pl_update(uint8_t devID, uint16_t V_IN, uint16_t I_IN, uint16_t V_OUT, uint16_t I_OUT, uint8_t Temp, uint8_t SOC, uint8_t SOH, uint8_t BF, uint8_t NW)
{
	struct participant_node *zeiger;
	zeiger = pl_getInfo(devID);
	if(zeiger == NULL)
	{
		pl_List_ErrorHandler(3);
		return;
	}
	else
	{
		if(zeiger->NW != NW)
		{
			/*Hier muss man neu einsortieren*/
			pl_delete(devID);
			pl_sort_insert(devID, V_IN, I_IN, V_OUT, I_OUT, Temp, SOC, SOH, BF, NW);
		}
		else
		{
			if(zeiger->V_IN != V_IN) zeiger->V_IN = V_IN;
			if(zeiger->I_IN != I_IN) zeiger->I_IN = I_IN;
			if(zeiger->V_OUT != V_OUT) zeiger->V_OUT = V_OUT;
			if(zeiger->I_OUT != I_OUT) zeiger->I_OUT = I_OUT;
			if(zeiger->Temp != Temp) zeiger->Temp = Temp;
			if(zeiger->SOC != SOC) zeiger->SOC = SOC;
			if(zeiger->SOH != SOH) zeiger->SOH = SOH;
			if(zeiger->BF != BF) zeiger->BF = BF;
		}
	}
}

void pl_delete(uint8_t devID)
{
	struct participant_node *zeiger, *zeiger1, *zeiger2;

	/*Ist Element vorhanden?*/
	if(anfang != NULL)
	{
		/*Ist es das erste Element?*/
		if(anfang->devID == devID)
		{
			zeiger=anfang->next;
			/*Besteht die Liste nur aus diesem Element?*/
			if(zeiger == NULL)
			{
				free(anfang);
				anfang=NULL;
				ende=NULL;
				return;
			}
			/*Ansonsten nachr�cken*/
			zeiger->previous = NULL;
			free(anfang);
			anfang = zeiger;
		}
		/*Ist es das letzte Element?*/
		else if(ende->devID == devID)
			{
				zeiger = ende->previous;
				zeiger->next = NULL;
				zeiger1 = ende;
				ende = zeiger1;
				free(zeiger1);
			}
			/*Suche das ELement in der Liste*/
			else
			{
				zeiger = anfang;
				while(zeiger->next != NULL)
				{
					zeiger1 = zeiger->next;
					/*Element ist gefunden und wird gel�scht*/
					if(zeiger1->devID == devID)
					{
						zeiger->next = zeiger1->next;
						zeiger2 = zeiger1->next;
						zeiger2->previous = zeiger;
						free(zeiger1);
						break;
					}
					zeiger = zeiger1;
				}
			}
	}
	else pl_List_ErrorHandler(3);
}

void pl_delete_all()
{
	struct participant_node *zeiger, *zeiger1;
	/*Falls liste vorhanden, l�sche alles*/
	if(anfang != NULL)
	{
		zeiger = anfang->next;
		/*Zuerst l�sche alles zwischen anfang und ende*/
		while(zeiger != NULL)
		{
			zeiger1 = anfang->next->next;
			if(zeiger1 == NULL) break;
			anfang->next = zeiger1;
			zeiger1->previous = anfang;
			free(zeiger);
			zeiger=zeiger1;
		}
		/*dann l�sche auch anfang und ende*/
		free(anfang);
		free(ende);
		anfang=NULL;
		ende=NULL;
	}
	else pl_List_ErrorHandler(2);
}

void pl_List_ErrorHandler(int error_ID)
{
	switch(error_ID)
	{
		case 0: //
			break;
		case 1: //Problem mit Memory reservation
			break;
		case 2: //Liste nicht gefunden
			break;
		case 3: //Element nicht gefunden
			break;
		default: //unbekanntes Problem
			break;
	}
}

/*----------------------------------------------------------------------------------------*/
/*--Functions Election Log----------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------*/

void el_start(void)
{
	el_anfang = el_ende = NULL;
}


void el_insert(uint8_t prioID, uint8_t devID, uint8_t data)
{
	struct election_log *zeiger, *zeiger1;
	/* Wurde schon ein Speicher f� den ende-Zeiger bereitgestellt?*/
	if(el_ende == NULL)
	{
		if((el_ende = malloc(sizeof(struct election_log))) == NULL)
		{
			el_List_ErrorHandler(1);
			return;
		}
	}
	/*Gibt es schon ein Element in der Liste?*/
	if(el_anfang == NULL)
	{
		/*Erstelle erstes Listenelement*/
		if((el_anfang = malloc(sizeof(struct election_log))) == NULL)
		{
			el_List_ErrorHandler(1);
			return;
		}
		el_anfang->prioID = prioID;
		el_anfang->devID = devID;
		el_anfang->data = data;
		el_anfang->next = NULL;
		el_ende = el_anfang;
		el_ende->previous = NULL;
	}
	/*Neues Element wird hinten angeh�ngt*/
	else
	{
		zeiger=el_anfang;
		while(zeiger->next !=NULL) zeiger=zeiger->next;
		/*Der neue Wert wird hinten an der Liste angeh�ngt*/
		if((zeiger->next = malloc(sizeof(struct election_log))) == NULL)
		{
			el_List_ErrorHandler(1);
			return;
		}
		zeiger1=zeiger;
		zeiger=zeiger->next;
		zeiger->prioID = prioID;
		zeiger->devID = devID;
		zeiger->data = data;
		zeiger->next = NULL;
		el_ende = zeiger;
		zeiger->previous = zeiger1;
		zeiger1->next = zeiger;
	}
}

struct election_log* el_getInfo(uint8_t devID)
{
	struct election_log *zeiger, *zeiger1;
	if(el_anfang != NULL)
	{
		/*Ist es das erste Element?*/
		if(el_anfang->devID == devID)
		{
			zeiger = el_anfang;
			return zeiger;
		}
		/*Ist es das letzte Element?*/
		else if(el_ende->devID == devID)
			{
				zeiger = el_ende;
				return el_ende;
			}
			/*Suche Element in Liste*/
			else
			{
				zeiger = el_anfang;
				while(zeiger->next != NULL)
				{
					zeiger1 = zeiger->next;
					if(zeiger1->devID == devID)
					{
						return zeiger1;
					}
					zeiger = zeiger1;
				}
			}
	}
	else el_List_ErrorHandler(3);
	return NULL;
}


void el_delete_all()
{
	struct election_log *zeiger, *zeiger1;
	/*Falls liste vorhanden, l�sche alles*/
	if(el_anfang != NULL)
	{
		zeiger = el_anfang->next;
		/*Zuerst l�sche alles zwischen anfang und ende*/
		while(zeiger != NULL)
		{
			zeiger1 = el_anfang->next->next;
			if(zeiger1 == NULL) break;
			el_anfang->next = zeiger1;
			zeiger1->previous = el_anfang;
			free(zeiger);
			zeiger=zeiger1;
		}
		/*dann l�sche auch anfang und ende*/
		free(el_anfang);
		free(el_ende);
		el_anfang=NULL;
		el_ende=NULL;
	}
	else el_List_ErrorHandler(2);
}

void el_List_ErrorHandler(int error_ID)
{
	switch(error_ID)
	{
		case 0: //
			break;
		case 1: //Problem mit Memory reservation
			break;
		case 2: //Liste nicht gefunden
			break;
		case 3: //Element nicht gefunden
			break;
		default: //unbekanntes Problem
			break;
	}
}

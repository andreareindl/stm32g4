/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "fdcan.h"
#include "hrtim.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//Version2
#include "stdint.h"
#include "Reg_DCDC_digReg.h"
#include "BID_DCDC_newReg.h"
#include "DCDC_CAN_newReg.h"
#include "Reg_DCDC_newReg.h"
#include "INA228.h"
#include "config.h"
//Zustandsanzeige
#include "pb_encode.h"
#include "pb_decode.h"
#include "nachrichten.pb.h"
#include <stdbool.h>

//Leader Election
//#include "List.h"
//#include "stdlib.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
//Version2
typedef struct DCDC_OBJECT {
	float Rpot_value;
	float Vref;
	float I_set_percent;
	float I_CAN_sum;
	float V_set;
} DCDC_OBJECT;
static DCDC_OBJECT DCDCobj;
//Zustandsanzeige
#define BUFFER_SIZE 28
//Struct Leader Election
//struct ID {
//    uint8_t prio_ID;
//    uint8_t dev_ID;
//};
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BOARD1

#ifdef BOARD1
//#define BoardNr 0
#elif BOARD2
#define BOARDNR 1
#elif BOARD3
#define BOARDNR 2
#elif BOARD4
#define BOARDNR 3
#elif BOARD5
#define BOARDNR 4
#endif
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//Variablen Version2
volatile uint16_t adc1_buffer[2] = { 0, 0 };
volatile uint16_t adc2_buffer[2] = { 0, 0 };

uint8_t RxDataFromCAN[8]; // DLC=8
uint8_t TxDataToCAN[8]; //DLC=8

extern I2C_HandleTypeDef hi2c3;

float HVoltage = 0.0;
float LVoltage = 0.0;
float IOUT1 = 0.0;
float IOUT2 = 0.0;
uint8_t BoardNr = 0;//Wird in DCDC_CAN_newReg verwendet, lässt sich nicht überschreiben
//Zustandsanzeige
/* Buffer used for transmission */
bool newstatedisplay = false; //Variable ob neue Statusmeldungen gesendet werden müssen
bool status;
uint8_t aTxBuffer[BUFFER_SIZE];

////Variablen Leader Election
//int test=0;
//FDCAN_TxHeaderTypeDef  TxHeader;
//FDCAN_RxHeaderTypeDef  RxHeader;
//uint8_t TxData[64], RxData[64], mainFSM;
//struct participant_node *me, *leader=NULL, *assistant=NULL, *coassistant=NULL, *candidate=NULL, *Repl_Can;
//uint8_t election=0, election_accept[]={0,0,0}, election_lock=0, replacement=0, canisme=0;
//uint8_t s_election_accept=0, reject_candidate=0, reject_counter=0;
//uint8_t timing_lock=0, overtime=0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	DCDCobj.Rpot_value = 0.5;
	DCDCobj.Vref = 3.3;
	DCDCobj.I_set_percent = 0.2;
	DCDCobj.I_CAN_sum = 0;
	DCDCobj.V_set = 12;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C3_Init();
  MX_ADC2_Init();
  MX_DAC1_Init();
  MX_FDCAN2_Init();
  MX_TIM1_Init();
  MX_TIM5_Init();
  MX_FDCAN1_Init();
  MX_I2C4_Init();
  MX_SPI2_Init();
  MX_ADC1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_UART5_Init();
  MX_USART3_UART_Init();
  MX_HRTIM1_Init();
  /* USER CODE BEGIN 2 */
	// Init DCDC
  if (HAL_I2C_Init(&hi2c3) != HAL_OK)
  {
    Error_Handler();
  }
//  uint8_t SentTable[3];
//	SentTable[0] = 1;
//	SentTable[1] = 1;
//	SentTable[2] = 1;
//	  if (HAL_I2C_Master_Transmit(&hi2c3, 64, SentTable, 3, INA228_I2CTIMEOUT)!= HAL_OK)
//	  {
//	    Error_Handler();
//	  }

//	HAL_I2C_Master_Transmit(&hi2c3, 64, SentTable, 3, INA228_I2CTIMEOUT);
//	INA228_config(&hi2c3, INA228_LV);
// 	INA228_config(&hi2c3, INA228_HV);
//	 GETINA228_config(&hi2c3, INA228_LV);
	RESET_RELAIS();

  DCDC_INIT_PERIPHERAL();
	// DCDC BASIC STATE
	DCDC_BASIC_STATE();
	DCDC_MASTER_ON();

	HAL_Delay(300);
	DCDC_SET_OUT_VOLTAGE(DCDCobj.Vref);
	//  DCDC_CH2_OFF();
	DCDC_MODE_BUCK();


	//PC14 setzen
	DCDC_ANALOG_CONTROL();

//	DCDC_SET_LV_DROOP(DCDCobj.Rpot_value);
//	DCDC_SET_HV_DROOP(DCDCobj.Rpot_value);
	HAL_Delay(300);
	// -- Start the FDCAN Module
	if (HAL_FDCAN_Start(&hfdcan1) != HAL_OK) {
		// Show Error on Display
		while (1) {
		};
	}

	if (HAL_FDCAN_Start(&hfdcan2) != HAL_OK) {
		// Show Error on Display
		while (1) {
		};
	}

	HAL_Delay(100);

	// -- Enables ADC DMA request -- Start ADCs
	if (HAL_ADC_Start_DMA(&hadc1, (uint32_t*) adc1_buffer, 2) != HAL_OK) {
		// Show Error on Display
		while (1) {
		};
	}
	if (HAL_ADC_Start_DMA(&hadc2, (uint32_t*) adc2_buffer, 2) != HAL_OK) {
		// Show Error on Display
		while (1) {
		};
	}


	HAL_Delay(500);
	HAL_TIM_Base_Start_IT(&htim5);
	HAL_Delay(500);
	HAL_TIM_Base_Start_IT(&htim1);


    HAL_HRTIM_WaveformOutputStart(&hhrtim1, HRTIM_OUTPUT_TA1);
    HAL_HRTIM_WaveformCounterStart(&hhrtim1, HRTIM_TIMERID_TIMER_A);



	//Init the messages for the status display
	Powerflow powerflowmsg = Powerflow_init_zero;
	grid gridmsg = grid_init_zero;
	bar barmsg = bar_init_zero;
	Battery batterymsg = Battery_init_zero;
	election electionmsg = election_init_zero;
	powerflowmsg.msgid1 = 1;
	gridmsg.msgid2 = 2;
	barmsg.msgid3 = 3;
	batterymsg.msgid4 = 4;
	electionmsg.msgid5 = 5;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
//		  uint8_t SentTable[3];
//			SentTable[0] = 1;
//			SentTable[1] = 1;
//			SentTable[2] = 1;
//		  HAL_I2C_Master_Transmit(&hi2c3, 65, SentTable, 3, INA228_I2CTIMEOUT);
//		  HAL_Delay(1000);

		// Setpoint via CAN
//		receiveMyCanData(&hfdcan1, RxDataFromCAN);
//		if (RxDataFromCAN[0] == 0xAA) {
//			DCDCobj.I_set_percent = (float) RxDataFromCAN[1] / 100.0;
//			DCDCobj.I_CAN_sum = (float) ((((uint8_t) RxDataFromCAN[2]) << 8)
//					+ (((uint8_t) RxDataFromCAN[3]) << 0)) / 1000;
//			DCDCobj.V_set = (float) ((((uint8_t) RxDataFromCAN[4]) << 8)
//					+ (((uint8_t) RxDataFromCAN[5]) << 0)) / 1000;
//			RxDataFromCAN[0] = 0x00;
//			if (newstatedisplay) {
//				//Keine Powerflow direction
//				powerflowmsg.voltage = LVoltage;
//				powerflowmsg.current = IOUT1 + IOUT2;
//				gridmsg.voltage = HVoltage;
//				gridmsg.current = IOUT1 + IOUT2;
//				pb_ostream_t stream = pb_ostream_from_buffer(aTxBuffer,
//						sizeof(aTxBuffer));
//				status = pb_encode(&stream, Powerflow_fields, &powerflowmsg);
//				/* Then just check for any errors.. */
//				if (!status) {
//					printf("Encoding failed: %s\n", PB_GET_ERROR(&stream));
//					return 1;
//				}
//				if (HAL_UART_Transmit(&huart1, stream.bytes_written, sizeof(stream), 100) != HAL_OK) {
//					Error_Handler();
//				}
//				status = pb_encode(&stream, grid_fields, &gridmsg);
//				/* Then just check for any errors.. */
//				if (!status) {
//					printf("Encoding failed: %s\n", PB_GET_ERROR(&stream));
//					return 1;
//				}
//				if (HAL_UART_Transmit(&huart1, stream.bytes_written, sizeof(stream), 100) != HAL_OK) {
//					Error_Handler();
//				}
//				newstatedisplay = false;
//
//			}
		}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 36;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV3;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc) {
	// Called after ADC Conversion(float)
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
	HVoltage = adc2_buffer[0] * ((float)3.3 / (float)4096) * ((float)27 /(float) 3.3);
	LVoltage = adc2_buffer[1] * ((float)3.3 / (float)4096) * ((float)16 / (float)3.3);


	//TO DO: Pa5 auf ADC2 IN13 HV Reference Voltage
	// PC4 auf ADC2 IN5 LV Reference Voltage

//	DCDCobj.I_curr=IOUT1+IOUT2;
//	DCDCobj.I_curr+=DCDCobj.I_CAN_sum;
//
//	DCDCobj.Vdis=Load_Distribution_Cont(DCDCobj.I_CAN_sum, DCDCobj.V_set, DCDCobj.I_curr, DCDCobj.I_set_percent);
//
//
//	//pulse=	Voltage_Regulation_Dig_PI(DCDCobj.Vdis, LVoltage);
//	pulse=1000;
//
//	pulse=	Voltage_Regulation_Dig_PI(DCDCobj.V_set, LVoltage);
//
//	//DCDC_Analog_Regulation_Droop(DCDCobj.V_set, DCDCobj.I_curr);

	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].CMP1xR = 1000;

	newstatedisplay = true;
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim == &htim1) {


	} else if (htim == &htim5) {

//		float ILV = INA228_getCURRENT_A(&hi2c3, INA228_LV);
//		float IHV = INA228_getCURRENT_A(&hi2c3, INA228_HV);
//		float VLV= INA228_getVBUS_V(&hi2c3, INA228_LV);
//		float VHV = INA228_getVBUS_V(&hi2c3, INA228_HV);

		TxDataToCAN[0] = (uint8_t) (((uint16_t) ((IOUT1 + IOUT2) * 1000)) >> 8);
		TxDataToCAN[1] = (uint8_t) (((uint16_t) ((IOUT1 + IOUT2) * 1000)) >> 0);
		TxDataToCAN[2] = (uint8_t) (((uint16_t) (HVoltage * 1000.0)) >> 8); // in mV
		TxDataToCAN[3] = (uint8_t) (((uint16_t) (HVoltage * 1000.0)) >> 0); // in mV
		TxDataToCAN[4] = (uint8_t) (((uint16_t) (LVoltage * 1000.0)) >> 8); // in mV
		TxDataToCAN[5] = (uint8_t) (((uint16_t) (LVoltage * 1000.0)) >> 0); // in mV
		sendMyCanData(&hfdcan1, (0x100 + BoardNr), TxDataToCAN, 6);
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
